// @flow
import React, {
  Component
} from 'react';
import {
  Link
} from 'react-router';
import Tabs from './Tabs';
import StoreButton from './StoreButton';
import styles from './Collections.css';
import autoBind from "react-autobind";

export default class Collections extends Component {


  constructor() {
    super();
    this.state = {
      index: -1
    };
    this.buttons = [];
    autoBind(this);

  }


  keyMatcher(e) {
    console.log("KEY on tabs", e.keyCode);
    switch (e.keyCode) {
    case 13: // enter
      if (this.props.collections[this.state.index]) {
        //this.props.router.push("/");
        this.props.router.push("/collections/" + this.props.collections[this.state.index].name);
      }
      break;
    case 37:
      if (this.state.index <= 0) {
        this.setState({
          index: this.props.collections.length
        })
      } else {
        this.setState({
          index: this.state.index - 1
        })
      }
      break;
    case 39:
      if (this.state.index >= this.props.collections.length) {
        this.setState({
          index: 0
        })
      } else {
        this.setState({
          index: this.state.index + 1
        });
      }
      break;
    }
  }

  componentDidMount() {
    document.addEventListener("keyup", this.keyMatcher);
  }

  componentWillUnmount() {
    document.removeEventListener("keyup", this.keyMatcher);
  }

  render() {
    const items = this.props.collections.map((store, i) => {
      return (<StoreButton focus={this.state.index === i} className={styles.collection_item + (this.state.index === 1 ? ' active':'')} key={i}
ref= {(r)=>{this.buttons[i] = r}}
              name={store.name} store={store}/>)
    });
    return (
      <div className = {styles.container + ""} data-tid="container"  style={{overflow: 'hidden'}}>
            <div className={styles.collection_title}>Explore a collection</div>
            <div style={{ width: '50%',
    margin: 'auto'}}>
                              {items}
                </div> < /div>);
    }
  }
