// @flow
import React, {
  Component
} from 'react';
import autoBind from 'react-autobind';
import {
  Link
} from 'react-router';

export default class StoreButton extends Component {

  styles = {
    border: 'none',
    transition: "all 100ms linear",
    'height': 141,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  }

  constructor() {
    super();
    autoBind(this);
    this.state = {
      logo: ''
    };
  }

  componentDidMount() {
    this.setState({
      logo: this.props.store.logoWhite || this.props.store.logo
    });
    this.styles.backgroundImage = "url(" + this.props.store.logoWhite + ")";
    if (this.props.focus) {
      this.onFocus();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.focus !== nextProps.focus) {
      if (nextProps.focus) {
        this.onFocus();
        return;
      }
      this.onBlur();
    }
  }

  onFocus(e) {
    this.setState({
      logo: this.props.store.logoActive
    });
    this.styles.height = 235;
  }

  onBlur(e) {
    this.setState({
      logo: this.props.store.logoWhite
    });
    this.styles.height = 141;
  }

  onClick() {
    this.setState({
      store: this.props.store.name
    });
  }

  render() {
    return (<div><Link data-img={
        this.state.logo
      }
      className = {
        this.props.className
      }
      style={{...this.styles, ...{backgroundImage: 'url(' + this.state.logo + ')'} }}
      to = {
        {
          pathname: "/collections/" + this.props.store.name,
          state: {
            store: this.props.store
          },
          params: {
            store: this.props.store
          }
        }
      } ><img src={this.state.logo} style={{height:'100%',visibility: 'hidden'}}/></Link></div>);
  }
}
