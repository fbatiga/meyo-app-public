// @flow
import React, {
    Component
} from 'react';
import {
    Link
} from 'react-router';


export default class TimerPage extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.interval = false;

    }


    componentDidMount() {
        console.log(this.props);
        this.setState({
            count: this.props.count || 5
        });
    }

    start() {
        this.setState({
            count: 5
        });
        this.interval = setInterval(() => {
            if (this.state.count <= 0) {
                clearTimeout(this.interval);
                return;
            }
            this.setState({
                count: this.state.count - 1
            });
        }, 1000);
    }

    reset() {
        this.setState({
            count: 5
        });
    }
    render() {
        return (
            <div>
            <div className={this.props.className} style={{...{position:'absolute', zIndex:100, top:0, left:0, width: '100%'},...this.props.style}}>
                <div style={{width:'100%', textAlign:'center', marginTop:666, fontFamily: 'Alegreya-thin',
                fontSize: 480,   fontWeight: 100,   letterSpacing: 100, color: '#C8B22C'
            }} >
            {this.state.count}
            </div>
            </div> < /div>
        )
    }
}
