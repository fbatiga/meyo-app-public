// @flow
import React, {
    Component
} from 'react';
import {
    Link
} from 'react-router';
import Tabs from './Tabs';
import StoreButton from './StoreButton';
import autoBind from 'react-autobind';
import styles from './Collections.css';
import Carousel from 'react-3d-carousel';
import $ from 'jquery';


var currdeg = 0,
    carousel = $('.carousel'),
    carouselStyle = {
        "transform": "rotateY(" + currdeg + "deg)"
    };


export default class CollectionDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showGuide: false,
            showDress: false,
            currentDress: this.props.collection.items[0] || {},
            imageIndex: 0,
            images: this.props.collection.items,
            width: 400,
            layout: 'classic',
            ease: 'linear',
            duration: 400,
            currdeg: 0,


        };
        autoBind(this);

        this.imagesStyle = [];

        let ln = this.props.collection.items ? this.props.collection.items.length : 1;
        this.ry = 360 / (this.state.images.length);
        this.tz = Math.round((250 / 2) /
            Math.tan(Math.PI / (ln)));
    }


    /**
    @deprecated
    */
    rotate(direction) {
        var forward = direction === 'next';
        //  carouselStyle.transform = "rotateY(" + currdeg + "deg)";

        carouselStyle.transform = "translate(" + (forward ? -250 : 250) + "deg)";
        this.setState({
            carouselStyle: carouselStyle
        });
        setTimeout(() => {
            let $active = $('.item.active');
            let index = 0;

            if (forward) {
                let $next = $active.next();
                if ($next.length > 0) {
                    $active.removeClass("active");
                    $next.removeClass('before after').addClass('active');
                    var $after = $next.next();
                    $after.removeClass('before').addClass("after")
                    $active.addClass('before');
                }

            } else {
                let $prev = $active.prev();
                if ($prev.length > 0) {
                    $active.removeClass("active");
                    $prev.addClass('active').prev().removeClass('after').addClass("before");
                    $active.addClass('after');
                }

            }
        }, 100);
    }

    rotateLeft() {
        currdeg -= this.ry;
        carouselStyle.transform = "rotateY(" + (currdeg) + "deg)";
        return;
    }


    rotateRight() {
        currdeg += this.ry;
        carouselStyle.transform = "rotateY(" + (currdeg) + "deg)";
        return;
    }


    keyMatcher(e) {
        console.log("KEY", e.keyCode);
        switch (e.keyCode) {
        case 27: //echap
            this.toggleGuide(false);
            this.hideDress();
            break;
        case 13: // enter
            this.showDress();
            return;
            if (this.state.showGuide) {
                this.toggleGuide(false);
            } else if (this.state.showDress) {
                this.hideDress();
            } else {
                this.showDress();
            }

            break;
        case 37:
            this.previous();
            break;
        case 39:
            this.next();
            break;
        }
    }

    componentDidMount() {
        currdeg = 0;
        carouselStyle.transform = "rotateY(" + (currdeg) + "deg)";
        document.addEventListener("keyup", this.keyMatcher);
        setTimeout(() => {
            this.setState({
                show: true
            });
        }, 300);
    }

    componentWillUnmount() {
        document.removeEventListener("keyup", this.keyMatcher);
    }

    toggleGuide(on) {
        this.setState({
            showGuide: on !== undefined ? on : !this.state.showGuide
        });
    }

    showDress(event) {
        if (this.props.pageMode === "favorites") {
            return;
        }
        let idx = event ? event.target.getAttribute('data-key') : this.state.imageIndex;
        // let dress = event ? this.props.collection.items[event.target.getAttribute('data-key')] : this.props.collection.items[this.state.imageIndex];
        if (idx == undefined) {
            return;
        }
        this.props.router.push("/collections/" + this.props.collection.name + "/item/" + idx, {
            collection: this.props.collection,
            itemId: idx
        });
        return;
        this.toggleGuide(true);
        setTimeout(() => {
            this.setState({
                showDress: true,
                currentDress: dress
            });
        }, 500);
    }

    hideDress() {
        this.setState({
            showDress: false
        });
    }

    previous() {

        this.rotateRight();
        let idx = this.state.imageIndex - 1;
        if (idx < 0) {
            idx = this.props.collection.items.length - 1;
        }
        this.setState({
            imageIndex: idx,
            currentDress: this.props.collection.items[idx]
        })
    }


    next() {

        this.rotateLeft();
        let idx = this.state.imageIndex + 1;
        if (idx >= this.props.collection.items.length) {
            idx = 0;
        }
        this.setState({
            imageIndex: idx,
            currentDress: this.props.collection.items[idx]
        });
    }

    render() {
        var images = this.props.collection.items.map((image, idx) => {
            return (<div key={idx}  data-key={idx} className={"item " + (idx === this.state.imageIndex ? ' active ' :"") }
style={{ transform: ' rotateY(' + (idx * this.ry) + 'deg) translateZ(' + this.tz + 'px)' }}
                    ><img src={image.url} data-key={idx} alt="" onClick={(evt)=> { this.showDress(evt)}} /></div>);
        });
        return (<div style={{overflow: 'hidden', height: "100vh"}}>
                    <div id="collectionDetails" style={{"textAlign": "center"}} data-tid="container" >
                            <div className={styles.collection_title} style={{visibility: 'hidden'}}>Welcome in {this.props.collection.label} collection</div>
                                <div className={"carousel-container fade " + (this.state.show ? " in": "") }>
                                    <div className="carousel" style={carouselStyle}>
                                    {images}
                                    </div>
                                </div>
                            </div>
                            <div className="footer-logo"><img src={this.props.collection.logoWhite || this.props.collection.logo} />
                            <span className="title">{this.state.currentDress.name || ""}</span>
                            </div>
                        </div>);
    }
}
