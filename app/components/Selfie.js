// @flow
import autobind from 'autobind-decorator';
import React, {
    Component
} from 'react';
import {
    Link
} from 'react-router';
import autoBind from 'react-autobind';

import Tabs from './Tabs';
import ImagePage from './ImagePage';
import Notification from './Notification';
import TimerPage from './TimerPage';
import styles from './Home.css';
import WebCamera from "webcamjs";
import SockJS from 'sockjs-client';

const electron = require('electron').remote;
const app = electron.app;

var fs = require('fs');
let gm = require("gm");
export default class Selfie extends Component {

    constructor() {
        super();

        this.imagesFolder = app.getPath('documents') + "/meyo";
        this.state = {
            displayMode: "selfie",
            showTimer: false,
            showShareResult: false,
            cameraFeed: true
        };
        this.anim = {};
        this.currentImageBuffer = null;
        autoBind(this);


        if (!fs.existsSync(this.imagesFolder)) {
            fs.mkdirSync(this.imagesFolder);
        }

        sessionStorage.setItem("meyo_selfie_firstTime", 0);
    }

    componentDidMount() {
        this.startWebcamjs();
        document.addEventListener("keyup", this.keyMatcher);

        this.sockBackend = new SockJS('http://' + window.location.hostname + ':9999/voice');
        console.log(this.sockBackend);
        this.sockBackend.onopen = function () {
            console.log("backend open")
        };

        console.log(this.sockBackend);
        this.sockBackend.onmessage = (e) => {
            console.log('backend message', e.data, typeof (e.data));
            this.handleVoiceCommands(e.data);
        };

        this.askForSelfie()

    }

    componentWillUnmount() {
        document.removeEventListener("keyup", this.keyMatcher);
        this.sockBackend.close();
        sessionStorage.setItem("meyo_selfie_firstTime", false);
    }

    /** binds keyboard events to actions in the app*/
    keyMatcher(e) {
        console.log(e.keyCode);
        switch (e.keyCode) {
        case 27: //echap
            break;
        case 37: //prev
            break;
        case 39: //next
            break;
        case 80: // p => picture
            this.takePicture();
            break;
        case 13: // enter
            if (this.state.showShareResult) {
                this.setState({
                    showShareResult: false
                });
            } else {
                this.takePicture();
            }
            break;

        case 83: // s => save || Share
            this.saveImageAndShare();
            break;
        case 87: // w => watermark
            this.watermarkImage();
            break;
        }
    }

    // handle voice commands
    handleVoiceCommands(dataInfo: any) {
        console.log("VOICE ENDPOINT HAS DATA", typeof (dataInfo), JSON.parse(dataInfo), dataInfo.key);
        let parsed = JSON.parse(dataInfo);

        console.log("VOICE ENDPOINT HAS DATA", parsed, parsed.key);

        switch (parsed.key) {
        case 'selfie':
        case 'new':
            if (this.state.displayMode !== "comment") {
                console.log("taking picture")
                this.takePicture()
            }
            break;
        case 'comment':
            if(this.state.displayMode == "comment"){
                console.log('watermarking image')
                this.watermarkImage()
            }
            break;
        case 'share':
            if(this.state.displayMode == "comment"){
                this.saveImageAndShare()
            }
            break;
        }
    }

    startWebcamjs() {
        console.warn("WEBCAM", WebCamera, WebCamera.userMedia);
        setTimeout(() => {
            WebCamera.on("error", () => {
                this.notificationLeft.showNotification('AN ERROR OCCURED WHILE STARTING THE CAMERA');
            });
            WebCamera.mediaDevices.enumerateDevices().then((devices) => {
                WebCamera.attach('#camera-container');
            });
        }, 2000);
    }

    //ask for a selfie
    askForSelfie(){
        let timer = 1000;
        setTimeout(() => {
            this.setState({
                showAnimation: 0
            });
            this.notificationLeft.showNotification('YOU LOOK GREAT<br/>TAKE A SELFIE');
            setTimeout(() => {
                this.notificationRight.showNotification("<span class='small'>SAY</span><br/>SELFIE !", {
                    "icon": "assets/img/icons/microphone.png"
                });
            }, 1200);
        }, timer);
    }

    //  @autobind
    takePicture() {
        var that = this;

        this.notificationRight.clearNotification();
        this.notificationLeft.clearNotification();
        this.timer.start();
        this.setState({
            showTimer: true,
            picture: "",
            cameraFeed: true
        });
        setTimeout(() => {
            this.setState({
                showTimer: false
            });
            this.timer.reset();
            WebCamera.snap((data_uri) => {
                // Save the image in a variable
                this.currentImageBuffer = this.processBase64Image(data_uri);

                let fileName = this.imagesFolder + "/" + Date.now() + ".jpg";
                gm(this.currentImageBuffer.data).rotate("#000", -90).write(fileName, (err) => {
                    console.warn("GM STREAM WRITE", fileName, err);
                    if (err) {
                        console.error("could not save file", err);
                        this.notificationLeft.showNotification("SORRY I DIDN'T MANAGE<br/>TO SAVE YOUR PICTURE", {
                            timeout: 3000
                        });
                        return;
                    }
                    document.getElementById("shot").src = fileName;
                    this.setState({
                        picture: fileName,
                        cameraFeed: false
                    });
                    this.setState({
                        displayMode: "comment"
                    });

                    this.notificationLeft.showNotification('NICE ONE !<br/>ADD A COMMENT');
                    setTimeout(() => {
                        console.log("say someting ?");
                        this.notificationRight.showNotification("SAY SOMETHING", {
                            "icon": "assets/img/icons/microphone.png"
                        });
                    }, 500);
                });


                // fs.writeFile(fileName, this.currentImageBuffer.data, (err) => {

                // });


            });
        }, 5200);
    }

    processBase64Image(dataString) {
        var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
            response = {};
        if (matches.length !== 3) {
            return new Error('Invalid input string');
        }
        response.type = matches[1];
        response.data = new Buffer(matches[2], 'base64');

        return response;
    }

    watermarkImage() {
        let output = this.state.picture.replace(".jpg", "-signed.jpg");
        gm(this.state.picture).command('composite')
            .gravity('SouthEast')
            .out('-geometry', '+20+10') // offset
            .in(process.cwd() + '/app/assets/img/selfie/hello-paris.png')
            .write(output, (err) => {
                if (err) {
                    console.error("ERROR while watermarking picture", err);
                    return;
                }

                this.notificationLeft.showNotification("SHARE ON YOUR PHONE", {
                    timeout: 3000
                });

                this.notificationRight.showNotification("<span class='small'>SAY</span><br/>SHARE", {
                    "icon": "assets/img/icons/microphone.png"
                });

                this.setState({
                    picture: output
                });
            });
    }

    saveImageAndShare(data) {
        var fileName = app.getPath('documents') + "/" + Date.now() + ".jpg";
        fs.writeFile(fileName, data, (err) => {
            if (err) {
                console.log("An error ocurred creating the file " + err.message);
                this.notificationLeft.showNotification('An error ocurred while saving the file.');
                return;
            }
            this.notificationLeft.clearNotification();
            this.notificationRight.clearNotification();
            this.setState({
                displayMode: "share"
            });
            this.setState({
                showShareResult: true
            });
            setTimeout(() => {
                this.askForSelfie();
                this.setState({
                    showShareResult: false,
                    cameraFeed: true,
                    displayMode: "selfie",
                    picture: ""
                });
            }, 2000);
        })
    }

    render() {
        console.log("RE_RENDER OF COMPONENT : SELFIE");
        return (<div>
                    <TimerPage ref={(ref)=>{this.timer = ref }} count="2" style={{display: (this.state.showTimer ? 'block': 'none') }}/>
                    <div className={styles.container + " container"} data-tid="container">
                        <Notification ref={(ref)=> { this.notificationLeft = ref;}} direction="left" content=""/>
                        <Notification ref={(ref)=> { this.notificationRight = ref;}} direction="right" content=""/>
                        <div id="camera-container" className={"fade"  + (this.state.cameraFeed ? ' in' :' ')}
                        style={{ margin:"0",top:850, left:-420, width:1920, height:1080, position: 'absolute',
                        transform: "rotate(270deg)"}}>
                        </div>
                        <div className="imageZone"  style={{padding:'10px' , position: 'absolute',
                         top:0, margin:"364px 182px", height:"100%"}}>
                         <img src={this.state.picture} id="shot" style={{width:"100%"}} alt="" />
                        </div> </div> <ImagePage className = { "fade " + (this.state.showShareResult ? 'in' : "")}
            img-url='assets/img/selfie/_share-confirmation.png'
            style={{
                    backgroundColor: '#000',
                    zIndex: 100
                }}
            /> </div>);
    }
}
