// @flow
import React, {
  Component
} from 'react';
import {
  browserHistory,
  Link
} from 'react-router';
import styles from './Tabs.css';
import Notification from './Notification';
import autoBind from "react-autobind";

export default class Tabs extends Component {

  constructor() {
    super();
    this.state = {
      index: -1,
      showConfirmExit: false,
      confirmExit: false,
    };
    autoBind(this);

  }

  keyMatcher(e) {
    console.log("KEY on tabs", e.keyCode);
    switch (e.keyCode) {
    case 8: //backspace
      this.setState({
        index: -1,
        showConfirmExit: !this.state.showConfirmExit
      })
      this.notificationLeft.showNotification('do you want to leave meyo ?');
      break;
    case 13: // enter
      let links = ["/collections",
        "/store-locator",
        "/selfie",
        "/favorites"
      ]
      if (this.state.showConfirmExit) {
        this.sleepMode();
        this.props.router.push({
          pathname: "/intro",
          manualPlay: false,
          state: {
            manualPlay: false,
            continue: false
          }
        });
      } else if (links[this.state.index]) {
        console.log("push", links[this.state.index], this.props.router);
        this.props.router.push(links[this.state.index])
      }

      break;
    case 37:
      if (this.state.index <= 0) {
        this.setState({
          index: 4
        })
      } else {
        this.setState({
          index: this.state.index - 1
        })
      }
      break;
    case 39:
      if (this.state.index >= 3) {
        this.setState({
          index: 0
        })
      } else {
        this.setState({
          index: this.state.index + 1
        })
      }
      break;
    }
  }

  componentDidMount() {
    document.addEventListener("keyup", this.keyMatcher);
  }

  componentWillUnmount() {
    console.log("unmount tabs events");
    document.removeEventListener("keyup", this.keyMatcher);

  }

  sleepMode() {
    sessionStorage.removeItem("meyo_selfie_firstTime");
    sessionStorage.getItem("meyo_user_bookmarks");
    const electron = require('electron').remote;
    const app = electron.app;
    var fs = require('fs');
    let deleteFolderRecursive = (path) => {
      if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach((file, index) => {
          let curPath = path + "/" + file;
          if (fs.lstatSync(curPath).isDirectory()) { // recurse
            deleteFolderRecursive(curPath);
          } else { // delete file
            fs.unlinkSync(curPath);
          }
        });
        fs.rmdirSync(path);
      }
    };

    deleteFolderRecursive(app.getPath('documents') + "/meyo");
  }

  render() {
    let confirmTab = (<div className={ (this.state.index === 0 ? 'active':'') + " tab"} style={{position:'fixed', right:0, top: 554}}>
                    <Link to="/veille">
                        <i className="menu-icon veille"></i>
                    </Link>
                </div>);
    return (<div>
                {this.state.showConfirmExit ? (confirmTab) :""}
                {this.state.showConfirmExit ? (<Notification ref={(ref)=> { this.notificationLeft = ref;}}
                direction="left" content="" style={{top: 530,maxWidth:800}}/>):""}

               <div className={ styles['tab-container'] + "row tab-container fade " + ( this.state.showConfirmExit ? "" : "in")}>
               <div className={ (this.state.index === 0 ? 'active': '') + " tab"}>
                                      <Link to="/collections">
                                          <i className="menu-icon collection"></i>
                                      </Link>
                                  </div>
                                  <div className = { (this.state.index === 1 ? 'active' : '') + " tab" } >
                         <Link to="/store-locator">
                                         <i className="menu-icon storelocator"></i>
                                      </Link> </div> <div className = {
                         (this.state.index === 2 ? 'active' : '') + " tab"
                       } >
                       <Link to="/selfie">
                                      <i className="menu-icon selfie"></i>
                                      </Link> < /div> < div className = {
                       (this.state.index === 3 ? 'active' : '') + " tab"
                   } >
                   <Link to="/favorites">
                                      <i className="menu-icon wishlist"></i>
                                      </Link>
                    </div>
                </div> < /div>);
    }
  }
