// @flow
import React, {
    Component
} from 'react';
import {
    Link
} from 'react-router';
import Tabs from './Tabs';
import StoreButton from './StoreButton';
import autoBind from 'react-autobind';
import styles from './Collections.css';
import $ from 'jquery';

export default class Tryout extends Component {
    constructor(props) {
        super(props);
        autoBind(this);

        this.bookmarks = sessionStorage.getItem("meyo_user_bookmarks") ? JSON.parse(sessionStorage.getItem("meyo_user_bookmarks")) : [];
        var isBookmarked = this.bookmarks.find((elm) => {return this.props.item.name == elm.name});
        this.state = {
            showGuide: false,
            showDress: false,
            width: 400,
            tabIndex: -1,
            maxTabIndex: 2,
            dimMenu: false,
            isBookmarked: isBookmarked  ? true: false
        };
        this.tabsActions = [];
        this.tabsActions.push(this.goToFavorites);
        this.tabsActions.push(this.goToStoreLocator);
        this.tabsActions.push(this.showDress);
        this.timer = false;

    }

    componentDidMount() {
        document.addEventListener("keyup", this.keyMatcher);
        this.resetTimer();
    }

    componentWillUnmount() {
        document.removeEventListener("keyup", this.keyMatcher);
        clearTimeout(this.timer);
        sessionStorage.setItem("meyo_user_bookmarks", JSON.stringify(this.bookmarks));
    }

    keyMatcher(e) {
        console.log("KEY", e, e.keyCode);
        this.resetTimer();
        switch (e.keyCode) {
        case 27: //echap
            this.toggleGuide(false);
            this.hideDress();
            break;
        case 13: // enter
            if (this.state.showGuide) {
                this.toggleGuide(false);
            } else if (this.state.showDress) {
                this.hideDress();
            } else {
                if (this.tabsActions[this.state.tabIndex]) {
                    this.tabsActions[this.state.tabIndex]();
                }

            }

            break;
        case 37:
            this.previous();
            break;
        case 39:
            this.next();
            break;
        }
    }

    execTabAction(tab) {
        this.tabs[tab].onClick();
        switch (tab) {
        case 0:
            break;
        case 1:
            break;
        case 2:
            break;

        }
    }

    goToStoreLocator() {
        this.props.router.push({
            pathname: "/store-locator",
            state: {
                store: "assets/img/map.png"
            }
        })
    }

    resetTimer() {
        this.setState({
            dimMenu: false
        });
        if (this.timer) {
            clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => {
            this.setState({
                dimMenu: true
            });

        }, 2000);
    }

    goToFavorites() {
        this.setState({
            isBookmarked: !this.state.isBookmarked
        });
        if (this.state.isBookmarked) {
            this.bookmarks.push(this.props.item);
        } else {
            let bookmark = this.bookmarks.find((elm) => {return this.props.item.name == elm.name});
            this.bookmarks.splice(this.bookmarks.indexOf(bookmark), 1);
        }
    }

    toggleGuide(on) {
        this.setState({
            showGuide: on !== undefined ? on : !this.state.showGuide
        });
    }

    showDress() {
        this.toggleGuide(true);
        setTimeout(() => {
            this.setState({
                showDress: true,
            });
        }, 500);
    }

    hideDress() {
        this.setState({
            showDress: false
        });
    }

    previous() {

        let idx = this.state.tabIndex - 1;
        if (idx >= 0) {
            this.setState({
                tabIndex: idx,
            })
        } else {
            this.setState({
                tabIndex: this.state.maxTabIndex
            })
        }
    }


    next() {
        let idx = this.state.tabIndex + 1;
        if (idx <= this.state.maxTabIndex) {
            this.setState({
                tabIndex: idx
            })
        } else {
            this.setState({
                tabIndex: 0
            })
        }
    }

    render() {
        return (<div >
                        <div id="collectionDetails" style={{textAlign:'center'}} data-tid="container" >
                            <img className={ 'fade ' + (this.state.showGuide ? ' m-fadeIn' : ' m-fadeOut') }
                            src="assets/img/guide-essayage.png" style={{position: 'absolute', top: 0, left: 0,  zIndex: 10, background:'#000'}}
                            alt="" />

                            <div className={ 'fade  ' + (this.state.showDress ? ' m-fadeIn' : ' m-fadeOut') }
                            style={{position: 'absolute', top: 0, left: 0, zIndex: 8, background:'#000', height:'100%'}}>
                            <img
                            src={(this.props.item.urlEssayage != "" ? this.props.item.urlEssayage : this.props.item.url)}
                            style={{position: 'relative', width: this.props.item.width || "100%", top: 398 ,
                             left: this.props.item.left  }}
                            alt="" />
                            </div>
                            <div className={ 'fade   m-fadeIn ' }
                            style={{transition: '200ms all ease-out', position: 'absolute', top: 0, left: 0, zIndex: 5, background:'#000', height:'100%'}}>

                                <img
                                src={( this.props.item.url)}
                                style={{position: 'relative',
                                 top:  398, left: 0,
                                 width: '65%'
                                   }}
                                alt="" />
                                <div className={"action-zone " + ( this.state.dimMenu ? " dim-menu" : "")} style={{transition: '200ms all ease-out'}}>
                                <Link className={(this.state.tabIndex === 0 ? 'active':'') + " tab icon-item"} to="/favorites">
                                   <i className={"menu-icon " + (this.state.isBookmarked ? "wishlist-full" : "wishlist")}></i>
                                </Link>
                                <Link  className={(this.state.tabIndex === 1 ? 'active':'') + " tab icon-item"} to="/store-locator">
                                   <i className="menu-icon storelocator"></i>
                                </Link>
                                <a  className={(this.state.tabIndex === 2 ? 'active':'') + " tab icon-item"} onClick={this.showDress}>
                                   <i className="menu-icon bonhomme"></i>
                                </a>
                            </div>
                            <div className="footer-logo"><img src={this.props.collection.logoWhite || this.props.collection.logo} />
<span className="title">{this.props.item.name || "Skateur dress"}</span>
                            </div>
                        </div>
                        </div>
                </div>);
    }
}
