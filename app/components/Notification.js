// @flow
import React, {
  Component
} from 'react';
import
autoBind
from "react-autobind";
import {
  Link
} from 'react-router';

let styles = {
  notification: {
    fontFamily: 'Alegreya-thin',
    fontSize: 48,
    zIndex: 1000,
    minWidth: 330,
    maxWidth: 400,
    overflow: "hidden",
    position: 'absolute',
    opacity: 1,
    top: 182,
    textAlign: 'left',
    transition: "all 400ms ease-out"
  },
  notification_left: {
    color: '#C8B22C',
    left: 0,
    transform: 'translate3d(-400px, 0, 0)',

  },
  notification_right: {
    color: '#fff',
    right: 0,
    transform: 'translate3d(400px, 0, 0)',
  },
  notification_body_left: {

  },
  notification_body_right: {

  }
};

export default class Notification extends Component {

  constructor() {
    super();
    this.state = {};
    autoBind(this);
  }

  componentDidMount() {

  }

  componentWillReceiveProps(nextProps) {
    var newState = {}
    for (var i in nextProps) {
      if (this.state[i] !== nextProps[i]) {
        newState[i] = nextProps[i]
      }
    }
    this.setState(newState);
  }

  showNotification(text, options = {
    icons: false,
    timeout: false
  }) {
    console.log("RE-RENDER for notification", text);
    this.clearNotification();
    if (this.props.direction === "right") {
      styles.notification_right.transform = 'translate3d(0,0,0)';
      // styles.notification_right.opacity = 1;
    } else {
      styles.notification_left.transform = 'translate3d(0,0,0)';
      //styles.notification_left.opacity = 1;
    }
    this.setState({
      text: text,
      icon: options.icon
    });
    if (this.props.timeout) {
      console.log("TIMEOUT CLEARING REQUESTED ! ");
      setTimeout(() => {
        this.clearNotification();
      }, this.props.timeout);
    }
  }

  clearNotification() {
    console.info("CLEARING NOTIFICATION");
    this.setState({
      text: undefined,
      icon: false
    });
    if (this.props.direction === "right") {
      styles.notification_right.transform = 'translate3d(400px,0,0)';
    } else {
      styles.notification_left.transform = 'translate3d(-400px,0,0)';
    }
  }

  render() {
    return (
      <div className="notification-zone" data-direction={this.props.direction} style={{...styles.notification, ...(this.props.direction === 'right' ? styles.notification_right : styles.notification_left ) ,...this.props.style }}>
            {this.state.icon ? <div className="glow" style={{float:'left', height:'100%', width:43, marginTop: 20, marginRight:40 }}><img src={this.state.icon} style={{width:'100%' }}/></div> : ""}
            <div dangerouslySetInnerHTML={{__html:this.state.text || this.props.content}}></div>
      </div>
    );
  }
}
