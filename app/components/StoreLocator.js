// @flow
import React, {
    Component
} from 'react';
import {
    Link
} from 'react-router';
import Tabs from './Tabs';
import styles from './StoreLocator.css';


export default class StoreLocator extends Component {

    constructor(props) {
        super(props);
        this.state = {
            show: false
        }
    }
    componentDidMount() {
        setTimeout(() => {
            this.setState({
                show: true
            });
        }, 300);
    }

    render() {
        return (<div>
            <div className = {styles.container + " container"} data-tid="container" >
                <img src={this.props.store || 'assets/img/map-all.png'} className={"background-image fade " + (this.state.show ? "in": "")} />
                </div>
            </div>);
    }
}
