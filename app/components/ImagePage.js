// @flow
import React, {
    Component
} from 'react';
import {
    Link
} from 'react-router';


export default class ImagePage extends Component {
    constructor() {
        super();

    }
    componentDidMount() {
        console.log(this.props);
    }
    render() {
        return (
            <div className={this.props.className} style={{...{position:'absolute', top:0, left:0, width: '100%'},...this.props.style}}>
                <img src={this.props['img-url']}  style={{width:'100%', zIndex: 2, opacity: 1}} />
                </div>
        );
    }
}
