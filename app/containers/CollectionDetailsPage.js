// @flow
import React, {
    Component
} from 'react';
import {
    connect
} from 'react-redux';
import CollectionDetails from '../components/CollectionDetails';

const mapStateToProps = (state, ownProps) => {
    console.log(" received props FOR MAPPING", ownProps, state, state.collections.find((x) => {
        return x.name === ownProps.params.collectionName
    }));
    return {
        id: ownProps.params.id,
        firstLaunch: state.firstLaunch || true,
        collections: state.collections,
        collection: state.collections.find((x) => {
            return x.name === ownProps.params.collectionName
        })
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        onCollectionClick: (id) => {
            dispatch(id)
        }
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(CollectionDetails);
