// @flow
import React, {
    Component
} from 'react';
import CollectionDetails from '../components/CollectionDetails';

export default class FavoritesPage extends Component {
    constructor() {
        super();

        this.collection = {
            name: 'eclaireur',
            label: "L'Eclaireur",
            logoActive: "assets/img/logos/leclaireur-or.png",
            logoWhite: "assets/img/logos/leclaireur-white.png",
            logo: "assets/img/logos/leclaireur-filaire.png",
            items: []
        };
        this.collection.items = sessionStorage.getItem("meyo_user_bookmarks") ? JSON.parse(sessionStorage.getItem("meyo_user_bookmarks")) : [];
    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }


    render() {
        return (
            <CollectionDetails collection={this.collection} router={this.props.router} pageMode="favorites"/>
        );
    }
}
