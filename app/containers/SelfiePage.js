// @flow
import React, {
    Component
} from 'react';
import Selfie from '../components/Selfie';
import {
    connect
} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        notifications: state.notifications
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onNotificationOpen: (id) => {
            dispatch((id))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Selfie);
