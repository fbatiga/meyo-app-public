// @flow
import React, {
  Component
} from 'react';
import type {
  Children
} from 'react';
import {
  browserHistory,
  Link
} from 'react-router';
import SockJS from 'sockjs-client';
import $ from 'jquery';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

export default class App extends Component {
  constructor() {
    super();
    document.addEventListener("keyup", this.onKeyUp.bind(this));
    console.info("MAIN APP LOADED");
    this.sockBackend = new SockJS('http://' + window.location.hostname + ':9999/backend');
    setTimeout(() => {
      this.sockBackend.onopen = function () {
        console.info("BACKEND EVENT");
      };

      this.sockBackend.onmessage = (e) => {
        console.info('BACKEND MESSAGE', e.data);
        this.handleData(e.data);
      };
    }, 200);
  }


  props: {
    children: Children
  };


  dispatchKeypress(keyCode = 40, shiftKey) {
    var oEvent = document.createEvent('KeyboardEvent');

    // Chromium Hack
    Object.defineProperty(oEvent, 'keyCode', {
      get: function () {
        return this.keyCodeVal;
      }
    });
    Object.defineProperty(oEvent, 'which', {
      get: function () {
        return this.keyCodeVal;
      }
    });

    if (oEvent.initKeyboardEvent) {
      oEvent.initKeyboardEvent("keyup", true, true, document.defaultView, false, false, false, false, keyCode, keyCode);
    } else {
      oEvent.initKeyEvent("keyup", true, true, document.defaultView, false, false, false, false, keyCode, 0);
    }

    oEvent.keyCodeVal = keyCode;

    if (oEvent.keyCode !== keyCode) {
      alert("keyCode mismatch " + oEvent.keyCode + "(" + oEvent.which + ")");
    }

    document.dispatchEvent(oEvent);
  }


  simulateKeyPress(character) {
    jQuery.event.trigger({
      type: 'keyup',
      which: character
    });
  }


  handleData(data) {
    console.info("Server command received", data.type);
    switch (data.type) {
    case 'swiperight':
      this.dispatchKeypress(39);
      break;
    case 'tap':
      this.dispatchKeypress(13);
      break;
    case 'swipeleft':
      this.dispatchKeypress(37);
      break;
    case 'taphold':
      this.dispatchKeypress(8);
      break;
    }
  }

  onKeyUp(event) {
    switch (event.keyCode) {
    case 8:
      if (!this.props.router || this.props.router.location.pathname != "/home") {
        browserHistory.goBack();
      }
      // this.props.router.push("/");
      break;
    }
  }
  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}
