// @flow
import React, {
    Component
} from 'react';
import {
    connect
} from 'react-redux';
import Tryout from '../components/Tryout';

const mapStateToProps = (state, ownProps) => {
    let collection = state.collections.find((x) => {
        return x.name === ownProps.params.collectionName
    });
    let item = collection.items[parseInt(ownProps.params.itemId)];

    console.log("This is my new collection", ownProps, collection, item);
    return {
        id: ownProps.params.id,
        firstLaunch: state.firstLaunch || true,
        collectionName: ownProps.params.collectionName,
        itemId: ownProps.params.itemId,
        collection: collection,
        item: item
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        onCollectionClick: (id) => {
            dispatch(id)
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Tryout);
