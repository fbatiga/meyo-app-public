// @flow
import React, {
    Component
} from 'react';
import Home from '../components/Selfie';
import Tabs from '../components/Tabs';
import ImagePage from '../components/ImagePage';

let styles = {};

export default class HomePage extends Component {
    constructor() {
        super();

        styles.video = {
            'zIndex': 10,
            transition: "opacity 200ms linear",
            'textAlign': 'center',
            'marginTop': 0,
            'marginLeft': 0,
            opacity: () => {
                return this.state.showAnimation || 0
            },
            visibility: () => {
                return this.state.showAnimation ? 'visible' : 'hidden'
            }
        };

        this.state = {
            firstTime: true
        };


    }


    componentDidMount() {
        let firstTime = sessionStorage.getItem("meyo_selfie_firstTime") !== null ? +(sessionStorage.getItem("meyo_selfie_firstTime")) : true;
        console.log("homepage first time", firstTime);
        this.setState({
            firstTime: firstTime
        });
    }

    componentWillUnmount() {

    }

    render() {
        return (
            <div>
            <Tabs router={this.props.router}/>
            </div>
        );
    }
}
