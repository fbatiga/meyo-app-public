// @flow
import React from 'react';
import {
  Provider
} from 'react-redux';
import {
  Router,
  browserHistory,
  applyRouterMiddleware
} from 'react-router';
import routes from '../routes';
import {
  useTransitions,
  withTransition
} from 'react-router-transitions';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
type RootType = {
  store: {},
  history: {}
};
export default function Root({
  store,
  history
}: RootType) {
  return (
    <Provider store={store} >
      <Router history={history} routes={routes}
      render={applyRouterMiddleware(useTransitions({
            TransitionGroup: ReactCSSTransitionGroup,
            defaultTransition: {
              transitionName: 'page-transition',
              transitionAppear: true,
              transitionAppearTimeout: 500,
              transitionEnterTimeout: 400,
              transitionLeaveTimeout: 400
            }
          }))}

       />
    </Provider>
  );
}
