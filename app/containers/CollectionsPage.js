// @flow
import React, {
    Component
} from 'react'
import {
    bindActionCreators
} from 'redux'
import {
    connect
} from 'react-redux';
import {
    openCollection
} from '../actions/collections.js'
import Collections from '../components/Collections'

const mapStateToProps = (state) => {
    return {
        collections: state.collections
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onCollectionClick: (id) => {
            dispatch(openCollection(id))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Collections);
