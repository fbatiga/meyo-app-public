// @flow
import React, {
    Component
} from 'react';
import {
    Link
} from 'react-router';
import StoreLocator from '../components/StoreLocator';

export default class StoreLocatorPage extends Component {
    render() {
        return (
            <div>
            <StoreLocator store={this.props.location.state && this.props.location.state.store}/>
            </div>
        );
    }
}
