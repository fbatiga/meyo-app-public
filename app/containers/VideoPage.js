// @flow
import React, {
    Component
} from 'react';

import ImagePage from '../components/ImagePage';
import autoBind from "react-autobind";

let styles = {};

export default class VideoPage extends Component {
    constructor() {
        super();
        autoBind(this);
 this.state = {
     manualPlay: true,
     continue: true
 };
        styles.video = {
            'zIndex': 10,
            transition: "opacity 200ms linear",
            'textAlign': 'center',
            'marginTop': 0,
            'marginLeft': 0,
            opacity: () => {
                return this.state.showAnimation || 0
            },
            visibility: () => {
                return this.state.showAnimation ? 'visible' : 'hidden'
            }
        };
    }

    keyMatcher(e) {
        switch (e.keyCode) {
        case 13: // enter
            this.setState({
                continue: true
            });
            this.playVideo();
            break;
        case 37:
            break;
        case 39:
            break;
        }
    }


    componentDidMount() {
        document.addEventListener("keyup", this.keyMatcher);
        console.log("publci state", this.props.location, this.props.location.state, this.props.location && this.props.location.state && (this.props.location.state.continue + ""));
        this.setState({
            manualPlay: this.props.location.state ? this.props.location.state.manualPlay : true,
            continue: this.props.location.state ? this.props.location.state.continue : true
        },()=>{
            console.log(this.state);
            if (!this.state.manualPlay) {
                this.playVideo();
            }
        });

    }

    componentWillUnmount() {
        document.removeEventListener("keyup", this.keyMatcher);
    }

    playVideo() {
        var video = document.getElementById("video");
        video.currentTime = 0;
        video.play();
console.log("continue ?", this.state.continue);
        if (this.state.continue) {
            setTimeout(() => {
                console.log("pushing to home");
                sessionStorage.setItem("meyo_selfie_firstTime", 1);
                this.props.router.push("/home");
            }, 8000);
        }
    }


    render() {
        return (
            <div>
            <video id="video" width="100%" height="auto" style={styles.video}>
              <source src="assets/img/animation-meyo.mp4" type="video/mp4" />
            Your browser does not support the video tag.
            </video>
            </div>
        );
    }
}
