// @flow
export const SHARE = 'SHARE';
export const OPEN_COLLECTION = 'OPEN_COLLECTION';
export const SAVE = 'SAVE';

export function share() {
    return {
        type: SHARE
    };
}


export function openCollection(id) {
    return {
        type: SHARE,
        id: id
    };
}


export function save() {
    return {
        type: SAVE
    };
}
