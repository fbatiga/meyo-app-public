// @flow
import {
  SHARE,
  SAVE,
  OPEN_COLLECTION,
} from '../actions/collections';

export type counterStateType = {
  counter: number
};

type actionType = {
  type: string
};

export default function collection(state: array, action: actionType) {
  return [{
    name: 'eclaireur',
    label: "L'Eclaireur",
    logoActive: "assets/img/logos/leclaireur-or.png",
    logoWhite: "assets/img/logos/leclaireur-white.png",
    logo: "assets/img/logos/leclaireur-filaire.png",
    items: [{
      name: "Golden cuff bracelet",
      url: "assets/img/items/89700-1.png",
      urlEssayage: "assets/img/items/fitted/89700-2_essayage.png",
      width: '100%',
      top: 100,
      left: 'auto'
    }, {
      name: "Cocktail Dress",
      url: "assets/img/items/87912-1.png",
      urlEssayage: "assets/img/items/fitted/87912-3_essayage.png",
      width: '100%',
      top: 440,
      left: 'auto'
    }, {
      name: "Spring dress",
      url: "assets/img/items/88459-1.png",
      urlEssayage: "assets/img/items/88459-1.png",
      width: '100%',
      top: 398,
      left: 'auto'
    }, {
      name: "Lady bag",
      url: "assets/img/items/91443-6.png",
      urlEssayage: "assets/img/items/91443-2-essayage.png",
      width: '100%',
      top: 438,
      left: 'auto'
    }, {
      name: "Man Jacket",
      url: "assets/img/items/89145-1.png",
      urlEssayage: "assets/img/items/fitted/89145-3_essayage.png",
      width: '100%',
      top: 438,
      left: 'auto'
    }, {
      name: "Cocktail Shoes",
      url: "assets/img/items/photo_talon_01.png",
      urlEssayage: "assets/img/items/photo_talon_01.png",
      width: '100%',
      top: 398
    }, {
      name: "Suede Skirt",
      url: "assets/img/items/91445.png",
      urlEssayage: "assets/img/items/91445_essayage.png",
      width: '100%'
    }, {
      name: "Man shirt",
      url: "assets/img/items/89148-1.png",
      urlEssayage: "assets/img/items/fitted/89148-3_essayage.png",
      width: '100%',
      top: 100,
      left: 'auto'
    }, {
      name: "Saddle bag",
      url: "assets/img/items/91444.png",
      urlEssayage: "assets/img/items/91444-2-essayage.png"
    }, {
      name: "Skateur dress",
      url: "assets/img/items/louis-vuitton.png",
      urlEssayage: "assets/img/items/louis-vuitton--essayage.png",
      width: 0,
      height: 0
    }]
  }, {

    name: 'dior',
    label: 'Dior',
    logo: "assets/img/logos/dior-filaire.png",
    logoWhite: "assets/img/logos/dior-white.png",
    logoActive: "assets/img/logos/dior-or.png",

    items: [{
      url: "assets/img/items/88839-2.png",
      urlEssayage: "assets/img/items/88839-essayage.png",
      width: '100%',
      top: 398,
      left: 'auto'
    }, {
      url: "assets/img/items/89120-2.png",
      urlEssayage: "assets/img/items/89120-essayage.png",
      width: '100%',
      top: 398,
      left: 'auto'
    }, {
      url: "assets/img/items/91444.png",
      urlEssayage: "assets/img/items/91444-2-essayage.png",
      width: 0,
      height: 0
    }, {
      url: "assets/img/items/91443-3.png",
      urlEssayage: "assets/img/items/91443-2-essayage.png",
      width: '100%',
      top: 438,
      left: 'auto'
    }, {
      url: "assets/img/items/91443-6.png",
      width: '100%',
      top: 438,
      left: 'auto'
    }]
  }, {
    name: "chanel",
    label: "Chanel",
    items: [],
    logo: "assets/img/logos/chanel.png",
    logoActive: "assets/img/logos/chanel-or.png",
    logoWhite: "assets/img/logos/chanel-white.png",
  }, {
    name: "giorgio_armani",
    label: "Giorgio Armani",
    items: [],
    logoActive: "assets/img/logos/giorgio_armani-or.png",
    logo: "assets/img/logos/giorgio_armani.png",
    logoWhite: "assets/img/logos/giorgio_armani-white.png"
  }, {
    name: "gucci",
    label: "Gucci",
    items: [],
    logo: "assets/img/logos/gucci-filaire.png",
    logoActive: "assets/img/logos/gucci-or.png",
    logoWhite: "assets/img/logos/gucci-white.png",

  }, {
    name: "hermes",
    label: "Hermes",
    items: [],
    logoActive: "assets/img/logos/hermes-or.png",
    logoWhite: "assets/img/logos/hermes-white.png",
    logo: "assets/img/logos/hermes.png",
  }, {
    name: "louis_vuitton",
    label: "Louis Vuitton",
    items: [],
    logoActive: "assets/img/logos/louis_vuitton-or.png",
    logoWhite: "assets/img/logos/louis_vuitton-white.png",
    logo: "assets/img/logos/louis_vuitton.png",
  }, {
    name: "prada",
    label: "Prada",
    items: [],
    logoActive: "assets/img/logos/prada-or.png",
    logoWhite: "assets/img/logos/prada-white.png",
    logo: "assets/img/logos/prada.png",
  }, {
    name: "versace",
    label: "Versace",
    items: [],
    logoActive: "assets/img/logos/versace-or.png",
    logoWhite: "assets/img/logos/versace-white.png",
    logo: "assets/img/logos/versace.png",
  }];
}


export function getSelectedCollection(state: array, action: actionType) {

}
