// @flow
import {
    combineReducers
} from 'redux';
import {
    routerReducer as routing
} from 'react-router-redux';
import counter from './counter';
import collections from './collections';

const rootReducer = combineReducers({
    counter,
    collections,
    routing
});

export default rootReducer;
