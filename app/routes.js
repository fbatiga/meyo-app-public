// @flow
import React from 'react';
import {
  Router,
  Route,
  IndexRoute
} from 'react-router';
import {
  useTransitions,
  withTransition
} from 'react-router-transitions';
import App from './containers/App';
import Tabs from './components/Tabs';
import HomePage from './containers/HomePage';
import VideoPage from './containers/VideoPage';
import CollectionsPage from './containers/CollectionsPage';
import CollectionDetailsPage from './containers/CollectionDetailsPage';
import SelfiePage from './containers/SelfiePage';
import FavoritesPage from './containers/FavoritesPage';
import StoreLocatorPage from './containers/StoreLocatorPage';
import TryoutPage from './containers/TryoutPage';
import VeillePage from './containers/VeillePage';


export default (
  <div>
        <Route path="/" component={withTransition(App)}>
            <IndexRoute component={VideoPage}  key={'intro'} location={'intro'}/>
            <Route path="/home" component={HomePage}  key={'home'} location={'home'} />
            <Route path="/intro" location={'intro'} key={'intro'} component={VideoPage} />
            <Route path="/collections" location={'collections'} key={'collections'} component={CollectionsPage} />
            <Route path="/collections/:collectionName" key={'collections-details'} component={CollectionDetailsPage} />
            <Route path="/collections/:collectionName/item/:itemId" location={'collection-item'} key={'collection-item'}  component={TryoutPage} />
            <Route path="/store-locator" component={StoreLocatorPage} location={'store-locator'}  key={'store-locator'} />
            <Route path="/selfie" component={SelfiePage} key={'favorites'} key={'selfie'}  />
            <Route path="/favorites" component={FavoritesPage} key={'favorites'} key={'favorites'}  />
            <Route path="/veille" component={VeillePage} key={'veille'} key={'veille'}  />
      </Route>

  </div>
);
